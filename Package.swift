// swift-tools-version:5.3
import PackageDescription

let kitVersion = "1.1.0"
let checksum = "3527ad5d5f899c4d3bddfebc0e0a32afa6f1308a5ddcd880098a3d0d6fde4e0a"

let package = Package(
    name: "PlatiniumPushKit",
    platforms: [
        .iOS(.v11)
    ],
    products: [
        .library(
            name: "PlatiniumPushKit",
            targets: ["PlatiniumPushKitWrapper"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
    ],
    targets: [
        .target(
            name: "PlatiniumPushKitWrapper",
            dependencies: ["PlatiniumPushKit"]
        ),
        .binaryTarget(
            name: "PlatiniumPushKit",
            url: "https://gitea.openium.fr/platinium/ios-kit-binaries/releases/download/\(kitVersion)/PlatiniumPushKit.xcframework.zip",
            checksum: checksum
        ),
    ]
)
